<?php
    namespace Zimplify\SendGrid\Providers;
    use Zimplify\Cpre\Provider;
    use SendGrid\Mail\Mail;
    use SendGrid;

    /**
     * this adapter provide us to connect to SendGrid to send out messages.
     * Package: 1 (Core)
     * Type: 3 (Provider)
     * Item: 08 (SendGrid Provider)
     */
    class SendGridMailerProvider extends Provider {

        const ARGS_SETUP = "setup";        
        const FLD_AUTHORIZATION = "authorization";

        private $client;

        /**
         * sending the message
         * @param array $sender the sender info (0 = email, 1 = name)
         * @param array $receiver the sender info (0 = email, 1 = name)
         * @param string $body the HTML based data of the message body
         * @param string $title the title of the message
         * @param array $attachments the list of file code to extract from as attachments
         * @return bool
         */
        public function send(array $sender, array $receiver, string $body, string $title = "", array $attachments = []) : bool {
            $msg = new Mail();
            $msg->setFrom($sender[0], count($sender) > 1 ? $sender[1] : null);
            $msg->setTo($receiver[0], count($receiver) > 1 ? $receiver[1] : null);
            $msg->setSubject($title);
            $msg->addContent("text/html", $body);
            
            // dealing witht the contents
            foreach($attachment as $f) {
                $c = base64_encode(file_get_contents($f));
                $t = $n = "";
                $msg->addAttachment($c, $t, $n, "attachment");
            }

            // here is the real send
            try {
                $res = $this->client->send($msg);
                if ($res->statusCode() >= 200 && $res->statusCode() < 300) 
                    return true;
                else 
                    return false;
            } catch (Exception $ex) {
                $this->log($ex);
                return false;
            }
        }

        /**
         * startup initializer for the service
         * @return void
         */
        protected function initialize() {
            $this->client = new SendGrid($this->get(self::ARGS_SETUP)[self::FLD_AUTHORIZATION]);
        }

        /**
         * check if all startup arguments are available
         * @return bool
         */
        protected function isRequired() : bool {
            return $this->get(self::ARGS_SETUP) && array_key_exists(self::FLD_AUTHORIZATION, $this->get(self::ARGS_SETUP));
        }        
    }